// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Bomber.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Bomber, "Bomber" );

DEFINE_LOG_CATEGORY(LogBomber)
 