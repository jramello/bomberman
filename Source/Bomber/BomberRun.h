// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "BomberPickUp.h"
#include "BomberRun.generated.h"

/**
 * 
 */
UCLASS()
class BOMBER_API ABomberRun : public ABomberPickUp
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABomberRun();

	virtual void OnPickup(AActor* MyOverlappedActor, AActor* OtherActor) override;
};
