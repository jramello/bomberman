// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "GameFramework/Actor.h"
#include "BomberCharacter.h"
#include "BomberExtraBomb.h"
#include "BomberLongerBlast.h"
#include "BomberRCBomb.h"
#include "BomberRun.h"
#include "BomberMapTile.generated.h"

UENUM(BlueprintType)
enum class EWallType : uint8
{
	Destructible 	UMETA(DisplayName = "Destructible"),
	Indestructible 	UMETA(DisplayName = "Indestructible"),
	None			UMETA(DisplayName = "None")
};

UCLASS()
class BOMBER_API ABomberMapTile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberMapTile();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	class UStaticMeshComponent* Ground;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	class UStaticMeshComponent* Wall;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	class UDestructibleComponent* DestructibleMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tile")
	class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, AssetRegistrySearchable, Category = "Tile")
	EWallType WallType;

	UFUNCTION(BlueprintCallable, Category = "Tile")
	void Exploded(ABomberCharacter* Character);

	void InitializeWallType();

	TSubclassOf<class ABomberExtraBomb> PowerUpExtraBomb;

	TSubclassOf<class ABomberLongerBlast> PowerUpLongerBlast;

	TSubclassOf<class ABomberRun> PowerUpRun;

	TSubclassOf<class ABomberRCBomb> PowerUpRCBomb;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
