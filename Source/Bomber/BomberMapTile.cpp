// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberMapTile.h"
#include "Runtime/Engine/Classes/Components/DestructibleComponent.h"
#include "Runtime/Engine/Classes/Engine/DestructibleMesh.h"
#include "Components/StaticMeshComponent.h"
#include "BomberPlayerController.h"
#include "BomberHUD.h"

// Sets default values
ABomberMapTile::ABomberMapTile()
{
	WallType = EWallType::None;
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Ground = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	Wall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallComponent"));
	DestructibleMesh = CreateDefaultSubobject<UDestructibleComponent>(TEXT("DestructibleWall"));
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(GetRootComponent());

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube.1M_Cube'"));
	if(Mesh.Object)
	{
		Ground->SetupAttachment(SceneComponent);
		Ground->bCastStaticShadow = false;
		Ground->SetStaticMesh(Mesh.Object);
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> WallOb(TEXT("StaticMesh'/Game/Geometry/Meshes/1M_Cube.1M_Cube'"));
	if (WallOb.Object)
	{
		Wall->SetupAttachment(SceneComponent);
		Wall->bCastStaticShadow = false;
		Wall->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
		Wall->SetStaticMesh(WallOb.Object);
	}
	
	
	DestructibleMesh->SetupAttachment(SceneComponent);
	DestructibleMesh->bCastStaticShadow = false;
	DestructibleMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 100.0f));
	DestructibleMesh->SetCanEverAffectNavigation(true);
	
	static ConstructorHelpers::FObjectFinder<UClass> PowerUpExtraBombObj(TEXT("Blueprint'/Game/Blueprints/Items/BP_PowerUpExtraBomb.BP_PowerUpExtraBomb_C'"));
	if (PowerUpExtraBombObj.Object)
	{
		PowerUpExtraBomb = (UClass*)PowerUpExtraBombObj.Object;
	}
	static ConstructorHelpers::FObjectFinder<UClass> PowerUpLongerBlastObj(TEXT("Blueprint'/Game/Blueprints/Items/BP_PowerUpLongerBlast.BP_PowerUpLongerBlast_C'"));
	if (PowerUpLongerBlastObj.Object)
	{
		PowerUpLongerBlast = (UClass*)PowerUpLongerBlastObj.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> PowerUpRCBombObj(TEXT("Blueprint'/Game/Blueprints/Items/BP_PowerUpRCBomb.BP_PowerUpRCBomb_C'"));
	if (PowerUpRCBombObj.Object)
	{
		PowerUpRCBomb = (UClass*)PowerUpRCBombObj.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> PowerUpRunObj(TEXT("Blueprint'/Game/Blueprints/Items/BP_PowerUpRun.BP_PowerUpRun_C'"));
	if (PowerUpRunObj.Object)
	{
		PowerUpRun = (UClass*)PowerUpRunObj.Object;
	}
}

// Called when the game starts or when spawned
void ABomberMapTile::BeginPlay()
{
	Super::BeginPlay();

}

void ABomberMapTile::InitializeWallType()
{
	switch (WallType)
	{
	case EWallType::Destructible:
		Wall->DestroyComponent();
		break;
	case EWallType::Indestructible:
		DestructibleMesh->DestroyComponent();
		break;
	case EWallType::None:
		Wall->DestroyComponent();
		DestructibleMesh->DestroyComponent();
		break;
	}
}

// Called every frame
void ABomberMapTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomberMapTile::Exploded(ABomberCharacter* Character)
{
	//30% chances to drop a powerup on explode
	float RandResult = FMath::RandRange(0.0f, 1.0f);
	if (RandResult >= 0.7f)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Boom!"));
		FVector Location = GetActorLocation();
		Location.Z += 100.0f;

		int32 RandPickup = FMath::RandRange(0, 3);
		switch (RandPickup)
		{
			case 0: GetWorld()->SpawnActor<ABomberExtraBomb>(PowerUpExtraBomb, Location, FRotator::ZeroRotator, SpawnInfo); break;
			case 1: GetWorld()->SpawnActor<ABomberLongerBlast>(PowerUpLongerBlast, Location, FRotator::ZeroRotator, SpawnInfo); break;
			case 2: GetWorld()->SpawnActor<ABomberRun>(PowerUpRun, Location, FRotator::ZeroRotator, SpawnInfo); break;
			case 3: GetWorld()->SpawnActor<ABomberRCBomb>(PowerUpRCBomb, Location, FRotator::ZeroRotator, SpawnInfo); break;
			default: break;
		}
	}

	Character->GivePoints(1000);
	ABomberPlayerController* MyPC = Cast<ABomberPlayerController>(Character->GetController());
	if (MyPC)
	{
		ABomberHUD* MyHUD = Cast<ABomberHUD>(MyPC->GetHUD());
		if (MyHUD)
		{
			FVector Location = GetActorLocation();
			Location.Z += 200.0f;
			MyHUD->ShowPoints(1000, Location);
		}
	}
}