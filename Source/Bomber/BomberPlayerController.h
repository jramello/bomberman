// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Bomber.h"
#include "GameFramework/PlayerController.h"
#include "BomberPlayerController.generated.h"

UENUM(BlueprintType)
enum class EMovementDirection : uint8
{
	Up 		UMETA(DisplayName = "Up"),
	Down 	UMETA(DisplayName = "Down"),
	Right 	UMETA(DisplayName = "Right"),
	Left 	UMETA(DisplayName = "Left")
};

UCLASS()
class ABomberPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABomberPlayerController();

	void OnDropBomb();

	virtual void BeginPlay() override;

protected:
	FVector GetClosestTarget(EMovementDirection Direction);
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	void OnMoveRight(float Val);
	void OnMoveForward(float Val);
	void OnEscapePressed();

	FVector Destination;

	bool bMovingRight;
	bool bMovingForward;
};


