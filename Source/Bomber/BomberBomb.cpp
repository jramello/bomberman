// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberBomb.h"
#include "BomberCharacter.h"
#include "Bomber.h"

// Sets default values
ABomberBomb::ABomberBomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(GetRootComponent());

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetupAttachment(SceneComponent);
	MeshComponent->bCastStaticShadow = false;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("StaticMesh'/Game/Items/Bomb.Bomb'"));
	if (Mesh.Object)
	{
		MeshComponent->SetStaticMesh(Mesh.Object);
	}	
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECR_Block);
	
	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	ParticleSystemComponent->SetupAttachment(SceneComponent);
	ParticleSystemComponent->SetAutoActivate(true);
	ParticleSystemComponent->SecondsBeforeInactive = 0.0f;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particles(TEXT("ParticleSystem'/Game/Particles/Sparks.Sparks'"));
	if (Particles.Object)
	{
		ParticleSystemComponent->SetTemplate(Particles.Object);
	}

	static ConstructorHelpers::FObjectFinder<UClass> ExplosionOb(TEXT("Blueprint'/Game/Blueprints/Items/BP_Explosion.BP_Explosion_C'"));
	if (ExplosionOb.Object)
	{
		ExplosionBP = (UClass*)ExplosionOb.Object;
	}

	OnActorBeginOverlap.AddDynamic(this, &ABomberBomb::OnChainedExplosion);
	OnActorEndOverlap.AddDynamic(this, &ABomberBomb::SetCollisionBlocking);
}

// Called when the game starts or when spawned
void ABomberBomb::BeginPlay()
{
	OnDestroyed.AddDynamic(this, &ABomberBomb::Explode);
	Super::BeginPlay();
	if(!Cast<ABomberCharacter>(GetOwner())->HasRCBombs())
	{
		SetLifeSpan(2.0f);
	}
}

// Called every frame
void ABomberBomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomberBomb::Explode(AActor* DestroyedActor)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = GetOwner();
	//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Boom!"));

	FTransform MyTransform = FTransform(FRotator::ZeroRotator, GetActorLocation());
	SpawnedExplosion = GetWorld()->SpawnActorDeferred<ABomberExplosion>(ExplosionBP, MyTransform, GetOwner(), GetInstigator(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	UGameplayStatics::FinishSpawningActor(SpawnedExplosion, MyTransform);

	ABomberCharacter* CharacterOwner = Cast<ABomberCharacter>(GetOwner());
	if (CharacterOwner)
	{
		CharacterOwner->RecoverBomb();
	}
}

void ABomberBomb::OnChainedExplosion(AActor* MyOverlappedActor, AActor* OtherActor)
{
	if(Cast<ABomberExplosion>(OtherActor) && OtherActor != SpawnedExplosion)
	{
		Destroy();		
	}
}

void ABomberBomb::SetCollisionBlocking(AActor* MyOverlappedActor, AActor* OtherActor)
{
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Block);
	MeshComponent->SetCanEverAffectNavigation(true);
}