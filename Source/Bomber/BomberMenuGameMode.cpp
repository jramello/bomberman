// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberMenuGameMode.h"
#include "BomberHUD.h"

ABomberMenuGameMode::ABomberMenuGameMode()
{

	HUDClass = ABomberHUD::StaticClass();
	NumBots = 0;
}

void ABomberMenuGameMode::BeginPlay()
{
	Super::BeginPlay();
	RemoveSplitscreenPlayers();
	ABomberHUD* MyHUD = Cast<ABomberHUD>(UGameplayStatics::GetPlayerController(GetWorld(),0)->GetHUD());
	if (MyHUD)
	{
		MyHUD->ShowMainMenu();
	}
}

int32 ABomberMenuGameMode::GetNumBots()
{
	return NumBots;
}

void ABomberMenuGameMode::SetNumBots(int32 Num)
{
	NumBots = Num;
}

void ABomberMenuGameMode::RemoveSplitscreenPlayers()
{
	const int MaxSplitScreenPlayers = 4;
	ULocalPlayer* PlayersToRemove[MaxSplitScreenPlayers];
	int RemIndex = 0;

	for (FConstPlayerControllerIterator Iterator = GEngine->GameViewport->GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator)
	{
		// Remove only local split screen players
		APlayerController* Controller = *Iterator;
		if (Controller && Controller->IsLocalController() && !Controller->IsPrimaryPlayer())
		{

			ULocalPlayer* ExPlayer = Cast<ULocalPlayer>(Controller->Player);
			if (ExPlayer)
			{
				PlayersToRemove[RemIndex++] = ExPlayer;
				Controller->PawnPendingDestroy(Controller->GetPawn());
			}
		}
	}

	for (int i = 0; i < RemIndex; ++i)
	{

		GEngine->GameViewport->RemovePlayer(PlayersToRemove[i]);
	}

}