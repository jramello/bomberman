// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberLongerBlast.h"

ABomberLongerBlast::ABomberLongerBlast()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("StaticMesh'/Game/Geometry/Meshes/SM_PowerUpLongerBlast.SM_PowerUpLongerBlast'"));
	if (Mesh.Object)
	{
		MeshComponent->SetStaticMesh(Mesh.Object);
	}
}

void ABomberLongerBlast::OnPickup(AActor* MyOverlappedActor, AActor* OtherActor)
{
	Super::OnPickup(MyOverlappedActor, OtherActor);
	ABomberCharacter* OverlappedChar = Cast<ABomberCharacter>(OtherActor);
	if (OverlappedChar)
	{
		//Not in the parent class in order to add different sounds to each pickup
		USoundCue* Sound = (USoundCue*)StaticLoadObject(USoundCue::StaticClass(), nullptr, TEXT("/Game/Sounds/SC_Pickup.SC_Pickup"));
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
		OverlappedChar->IncrementBlastSize();
		Destroy();
	}
}



