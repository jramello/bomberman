// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberGameInstance.h"
#include "BomberMenuGameMode.h"
#include "TimerManager.h"

UBomberGameInstance::UBomberGameInstance()
{
	LastScore = 0;
}

void UBomberGameInstance::Init()
{
	Super::Init();

	// Register delegate for ticker callback
	TickDelegate = FTickerDelegate::CreateUObject(this, &UBomberGameInstance::Tick);
	TickDelegateHandle = FTicker::GetCoreTicker().AddTicker(TickDelegate);
	CurrentState = BomberGameInstanceState::None;
	StartGameInstance();
	SelectedMapName = "EmptyMap";
}

void UBomberGameInstance::ChangeMap(FString MapName, int32 Bots)
{
	FString Arguments = "?NumBots=" + FString::FromInt(Bots);
	UGameplayStatics::OpenLevel(GetWorld(), FName(*MapName), true, Arguments);
}


void UBomberGameInstance::NextRound(int32 Bots)
{
	FString Arguments = "?NumBots=" + FString::FromInt(Bots);
	UGameplayStatics::OpenLevel(GetWorld(), FName(*SelectedMapName), true, Arguments);

}

void UBomberGameInstance::Quit()
{
	UGameViewportClient* const Viewport = GetGameViewportClient();
	if (Viewport)
	{
		Viewport->ConsoleCommand("quit");
	}
}


void UBomberGameInstance::StartGameInstance()
{
	GotoInitialState();
}

bool UBomberGameInstance::Tick(float DeltaSeconds)
{
	// Dedicated server doesn't need to worry about game state
	if (IsRunningDedicatedServer() == true)
	{
		return true;
	}
	MaybeChangeState();
	return true;
}

#pragma region STATE MACHINE

void UBomberGameInstance::GotoState(FName NewState)
{
	PendingState = NewState;
}

void UBomberGameInstance::GotoInitialState()
{
	GotoState(GetInitialState());
}

FName UBomberGameInstance::GetInitialState()
{
	return BomberGameInstanceState::MainMenu;
}

void UBomberGameInstance::MaybeChangeState()
{
	if ((PendingState != CurrentState) && (PendingState != BomberGameInstanceState::None))
	{
		if (!bReadyToChange)
		{
			APlayerController* MyPC = GetFirstLocalPlayerController();
			if (MyPC)
			{
				if (!bFadingOut)
				{
					bFadingOut = true;
					MyPC->PlayerCameraManager->StartCameraFade(0.0f, 1.0f, 1.0f, FColor::Black, false, true);
					FTimerDelegate TimerDel;
					FTimerHandle TimerHandle;
					TimerDel.BindUFunction(this, FName("SetReadyToChange"), 1.0f);
					GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, 1.0f, false);
				}
			}
			else
			{
				SetReadyToChange();
			}
		}
		if (bReadyToChange)
		{
			FName const OldState = CurrentState;
			// end current state
			EndCurrentState(PendingState);
			// begin new state
			BeginNewState(PendingState, OldState);
			// clear pending change
			PendingState = BomberGameInstanceState::None;
			bReadyToChange = false;
		}
	}
}

void UBomberGameInstance::SetReadyToChange()
{
	bReadyToChange = true;
	bFadingOut = false;
}

void UBomberGameInstance::EndCurrentState(FName NextState)
{
	// per-state custom ending code here
	if (CurrentState == BomberGameInstanceState::MainMenu)
	{
		EndMainMenuState();
	}
	else if (CurrentState == BomberGameInstanceState::Playing)
	{
		EndPlayingState();
	}

	CurrentState = BomberGameInstanceState::None;
}

void UBomberGameInstance::BeginNewState(FName NewState, FName PrevState)
{
	// per-state custom starting code here
	if (NewState == BomberGameInstanceState::MainMenu)
	{
		BeginMainMenuState();
	}
	else if (NewState == BomberGameInstanceState::Playing)
	{
		BeginPlayingState();
	}
	CurrentState = NewState;
}

void UBomberGameInstance::BeginMainMenuState()
{
	ChangeMap("/Game/Maps/MenuMap", 0);
}

void UBomberGameInstance::EndMainMenuState()
{
}

void UBomberGameInstance::BeginPlayingState()
{
	int32 Bots = Cast<ABomberMenuGameMode>(GetWorld()->GetAuthGameMode())->GetNumBots();
	ChangeMap("/Game/Maps/" + SelectedMapName, Bots);
}

void UBomberGameInstance::EndPlayingState()
{

}

#pragma endregion


void UBomberGameInstance::SetSelectedMap(FString MapName)
{
	SelectedMapName = MapName;
}