// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BomberCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "BomberBomb.h"
#include "BomberPlayerController.h"
#include "BomberHUD.h"
#include "BomberGameMode.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"

ABomberCharacter::ABomberCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(10.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->MaxWalkSpeed = 300.0f;

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	NumBombs = 1;
	BlastSize = 2.0f;
}

void ABomberCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}


void ABomberCharacter::RCBombPickedUp()
{
	bRCBombs = true;
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;
	TimerDel.BindUFunction(this, FName("ClearRCBomb"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, 10.0f, false);
}

void ABomberCharacter::ClearRCBomb()
{
	//When player loses RC Bomb powerup, all his placed bombs will splode in 2s
	bRCBombs = false;
	for (TActorIterator<ABomberBomb> It(GetWorld()); It; ++It)
	{
		ABomberBomb* Bomb = *It;
		if (Bomb && Bomb->GetOwner() == this)
		{
			Bomb->SetLifeSpan(2.0f);
		}
	}
}

void ABomberCharacter::IncrementBlastSize()
{
	BlastSize++;
}

void ABomberCharacter::SetRunning()
{
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
}


bool ABomberCharacter::HasRCBombs() const
{
	return bRCBombs;
}

float ABomberCharacter::GetBlastSize() const
{
	return BlastSize;
}

bool ABomberCharacter::IsWalking() const
{
	return GetVelocity().Size() > 0.0f;
}

bool ABomberCharacter::IsDroppingBomb() const
{
	return bDroppingBomb;
}

void ABomberCharacter::SetDroppingBomb(bool bValue)
{
	if (!bValue)
	{
		FVector BombLocation = FVector::ZeroVector;
		FVector DownwardVector = FVector(0,0,-1);
		float Distance = 100.0f;
		FVector StartTrace = GetActorLocation();
		FVector EndTrace = StartTrace + DownwardVector * Distance;
		FHitResult Hit(ForceInit);
		FCollisionQueryParams TraceParams(FName(TEXT("ViewTrace")), true, this);
		TraceParams.bTraceAsyncScene = true;

		GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_Visibility, TraceParams);
		if (Hit.bBlockingHit)
		{
			FVector Origin;
			FVector BoxExtent;
			Hit.GetActor()->GetActorBounds(true, Origin, BoxExtent);
			BombLocation = FVector(Hit.GetActor()->GetActorLocation().X, Hit.GetActor()->GetActorLocation().Y, Hit.GetActor()->GetActorLocation().Z + BoxExtent.Z + 10.0f);
			//DrawDebugSphere(GetWorld(), Destination, 5, 10, FColor::Red, true);
		}
		FTransform MyTransform = FTransform(FRotator::ZeroRotator, BombLocation);
		ABomberBomb* Bomb = GetWorld()->SpawnActorDeferred<ABomberBomb>(ABomberBomb::StaticClass(), MyTransform, this, GetInstigator(), ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding);
		UGameplayStatics::FinishSpawningActor(Bomb, MyTransform);

		NumBombs--;
		bDroppingBomb = bValue;
	}
	else if(NumBombs > 0)
	{
		bDroppingBomb = bValue;
	}
}

void ABomberCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	FString AnimClassString = "Class'/Game/Animations/BomberAnimBlueprint.BomberAnimBlueprint.BomberAnimBlueprint_C'";
	UClass* AnimationClass = LoadObject<UClass>(NULL, *AnimClassString);
	GetMesh()->SetAnimInstanceClass(AnimationClass);
}

void ABomberCharacter::RecoverBomb()
{
	NumBombs++;
}

int32 ABomberCharacter::GetNumBombs()
{
	return NumBombs;
}

int32 ABomberCharacter::GetNumPoints()
{
	return Points;
}

void ABomberCharacter::GivePoints(int32 NumPoints)
{
	Points += NumPoints;
}

float ABomberCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
	ABomberCharacter* Causer = Cast<ABomberCharacter>(DamageCauser);
	if (Causer)
	{
		USoundCue* Sound = (USoundCue*)StaticLoadObject(USoundCue::StaticClass(), nullptr, TEXT("/Game/Sounds/SC_Dying.SC_Dying"));
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
		
		Causer->GivePoints(5000);
		ABomberGameMode* MyGameMode = Cast<ABomberGameMode>(GetWorld()->GetAuthGameMode());
		if (MyGameMode)
		{
			MyGameMode->EndingGame();
		}
	
	}
	//Disable input when player dies
	FInputModeUIOnly Mode;
	ABomberPlayerController* MyPC= Cast<ABomberPlayerController>(GetController());
	if(MyPC)
	{
		MyPC->SetInputMode(Mode);
	}
	bDying = true;
	return 0.0f;
}

bool ABomberCharacter::IsDying() const
{
	return bDying;
}

bool ABomberCharacter::HasWon() const
{
	return bWon;
}

void ABomberCharacter::SetWon(bool bValue)
{
	bWon = bValue;
}

void ABomberCharacter::Die()
{
	SetActorHiddenInGame(true);
}