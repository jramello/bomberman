// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberRCBomb.h"

ABomberRCBomb::ABomberRCBomb()
{
	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("StaticMesh'/Game/Geometry/Meshes/SM_PowerUpRCBomb.SM_PowerUpRCBomb'"));
	if (Mesh.Object)
	{
		MeshComponent->SetStaticMesh(Mesh.Object);
	}
}

void ABomberRCBomb::OnPickup(AActor* MyOverlappedActor, AActor* OtherActor)
{
	Super::OnPickup(MyOverlappedActor, OtherActor);
	ABomberCharacter* OverlappedChar = Cast<ABomberCharacter>(OtherActor);
	if (OverlappedChar)
	{
		//Not in the parent class in order to add different sounds to each pickup
		USoundCue* Sound = (USoundCue*)StaticLoadObject(USoundCue::StaticClass(), nullptr, TEXT("/Game/Sounds/SC_Pickup.SC_Pickup"));
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), Sound, GetActorLocation());
		OverlappedChar->RCBombPickedUp();
		Destroy();
	}
}
