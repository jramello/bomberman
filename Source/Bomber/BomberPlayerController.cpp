// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BomberPlayerController.h"
#include "AI/Navigation/NavigationSystem.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "BomberCharacter.h"
#include "Components/DestructibleComponent.h"
#include "BomberBomb.h"
#include "BomberMapTile.h"
#include "BomberHUD.h"
#include "BomberGameInstance.h"


ABomberPlayerController::ABomberPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::None;
	bMovingRight = false;
	bMovingForward = false;
}

void ABomberPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
}


void ABomberPlayerController::BeginPlay()
{
	Super::BeginPlay();
	FInputModeGameAndUI Mode;
	SetInputMode(Mode);
	if(UGameplayStatics::GetPlayerControllerID(this) == 0)
	{
		Cast<ABomberHUD>(GetHUD())->ShowIngameHUD();
	}
}

void ABomberPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();
	int32 Id = UGameplayStatics::GetPlayerControllerID(this);
	switch (Id)
	{
	case 0:
		InputComponent->BindAction("MainMenu", IE_Released, this, &ABomberPlayerController::OnEscapePressed);
		InputComponent->BindAxis("MoveForwardP1", this, &ABomberPlayerController::OnMoveForward);
		InputComponent->BindAxis("MoveRightP1", this, &ABomberPlayerController::OnMoveRight);
		InputComponent->BindAction("DropBombP1", IE_Released, this, &ABomberPlayerController::OnDropBomb);
		break;
	case 1: 

		InputComponent->BindAxis("MoveForwardP2", this, &ABomberPlayerController::OnMoveForward);
		InputComponent->BindAxis("MoveRightP2", this, &ABomberPlayerController::OnMoveRight);
		InputComponent->BindAction("DropBombP2", IE_Released, this, &ABomberPlayerController::OnDropBomb);
		break;

	default:
		break;
	}
}

void ABomberPlayerController::OnEscapePressed()
{
	UBomberGameInstance* MyGameInstance = Cast<UBomberGameInstance>(GetGameInstance());
	if (MyGameInstance)
	{
		MyGameInstance->GotoState(BomberGameInstanceState::MainMenu);
	}
}

void ABomberPlayerController::OnDropBomb()
{
	
	ABomberCharacter* MyPawn = Cast<ABomberCharacter>(GetPawn());
	if (MyPawn)
	{
		bool RCBombsExploded = false;
		//If player has RC bombs powerup, make his bombs explode instead of spawning a new one.
		if (MyPawn->HasRCBombs())
		{
			for (TActorIterator<ABomberBomb> It(GetWorld()); It; ++It)
			{
				ABomberBomb* Bomb = *It;
				if (Bomb && Bomb->GetOwner() == MyPawn)
				{
					RCBombsExploded = true;
					Bomb->Destroy();
				}
			}
		}
		if (!RCBombsExploded)
		{
			MyPawn->SetDroppingBomb(true);
		}
	}	
}

void ABomberPlayerController::OnMoveForward(float Value)
{
	//Move only in one direction at a time
	if (Value != 0 && !bMovingRight)
	{
		bMovingForward = true;
		APawn* const MyPawn = GetPawn();
		if (MyPawn)
		{
			UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
			FVector Destination = FVector::ZeroVector;
			if (Value > 0)
			{
				//Destination is always the center of the next tile
				Destination = GetClosestTarget(EMovementDirection::Up);
			}
			else
			{
				Destination = GetClosestTarget(EMovementDirection::Down);
			}
			
			float const Distance = FVector::Dist(Destination, MyPawn->GetActorLocation());
			if (NavSys)
			{
				NavSys->SimpleMoveToLocation(this, Destination);
			}
		}
	}
	else
	{
		bMovingForward = false;
	}
}

void ABomberPlayerController::OnMoveRight(float Value)
{
	if(Value != 0 && !bMovingForward)
	{
		bMovingRight = true;
		APawn* const MyPawn = GetPawn();
		if (MyPawn)
		{
			UNavigationSystem* const NavSys = GetWorld()->GetNavigationSystem();
			FVector Destination = FVector::ZeroVector;
			if (Value > 0)
			{
				Destination = GetClosestTarget(EMovementDirection::Right);
			}
			else
			{
				Destination = GetClosestTarget(EMovementDirection::Left);
			}
			
			float const Distance = FVector::Dist(Destination, MyPawn->GetActorLocation());
			// We need to issue move command only if far enough in order for walk animation to play correctly
			if (NavSys)
			{
				NavSys->SimpleMoveToLocation(this, Destination);
			}
		}
	}
	else
	{
		bMovingRight = false;
	}
}

FVector ABomberPlayerController::GetClosestTarget(EMovementDirection Direction)
{
	FVector Destination = FVector::ZeroVector;
	FVector ForwardVector = FVector::ZeroVector;
	FVector StartTrace = GetPawn()->GetActorLocation();
	StartTrace.Z += 200.0f;
	//Find the tile in your Right/Left/Up/Down
	switch (Direction)
	{
		case EMovementDirection::Right: StartTrace.Y = StartTrace.Y + 100.0f; break;
		case EMovementDirection::Left: StartTrace.Y = StartTrace.Y - 100.0f; break;
		case EMovementDirection::Up: StartTrace.X = StartTrace.X + 100.0f; break;
		case EMovementDirection::Down: StartTrace.X = StartTrace.X - 100.0f; break;
	}

	float Distance = 400.0f;
	
	FVector EndTrace = StartTrace + FVector(0.0f,0.0f,-1.0f) * Distance;
	//DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Red, false, 1.0f, 1, 4.0f);
	FHitResult Hit(ForceInit);
	FCollisionQueryParams TraceParams(FName(TEXT("ViewTrace")), true);
	TraceParams.bTraceAsyncScene = true;

	GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_MAX, TraceParams);
	//Bombs, characters indestructible tiles or destructible tiles are not valid destinations
	if (Hit.bBlockingHit && !((Cast<ABomberMapTile>(Hit.GetActor()) && Cast<ABomberMapTile>(Hit.GetActor())->WallType == EWallType::Indestructible) || Cast<UDestructibleComponent>(Hit.GetComponent()) || Cast<ABomberBomb>(Hit.GetActor()) || Cast<ABomberCharacter>(Hit.GetActor())))
	{		
		//Set the top-center of the tile found as Destination
		Destination = FVector(Hit.GetActor()->GetActorLocation().X, Hit.GetActor()->GetActorLocation().Y, Hit.GetActor()->GetActorLocation().Z + 50.0f);
		//DrawDebugSphere(GetWorld(), Destination, 5, 10, FColor::Red, true);
	}
	else if(Hit.bBlockingHit)
	{ 
		EndTrace = GetPawn()->GetActorLocation() + FVector(0.0f, 0.0f, -1.0f) * Distance;
		TraceParams.AddIgnoredActor(GetPawn());
		GetWorld()->LineTraceSingleByChannel(Hit, GetPawn()->GetActorLocation(), EndTrace, ECC_MAX, TraceParams);
		Destination = FVector(Hit.GetActor()->GetActorLocation().X, Hit.GetActor()->GetActorLocation().Y, Hit.GetActor()->GetActorLocation().Z + 50.0f);
	}
	return Destination;
}