// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "BomberExplosion.h"
#include "BomberBomb.generated.h"

UCLASS()
class BOMBER_API ABomberBomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberBomb();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bomb")
	class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Bomb")
	class UStaticMeshComponent* MeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ParticleSystem", meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* ParticleSystemComponent;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void OnChainedExplosion(AActor* MyOverlappedActor, AActor* OtherActor);

	UFUNCTION()
	virtual void SetCollisionBlocking(AActor* MyOverlappedActor, AActor* OtherActor);

	TSubclassOf<class ABomberExplosion> ExplosionBP;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
private:

	UPROPERTY()
	ABomberExplosion* SpawnedExplosion;

	UFUNCTION()
	void Explode(AActor* DestroyedActor);
	
};
