// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "BomberUserWidget.h"
#include "BomberEndGameUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class BOMBER_API UBomberEndGameUserWidget : public UBomberUserWidget
{
	GENERATED_BODY()
	
public:

	//-1 means draw
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget")
	int32 WinnerId;
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Widget")
	void UpdateWidget();
};
