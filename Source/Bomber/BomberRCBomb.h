// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "BomberPickUp.h"
#include "BomberRCBomb.generated.h"

/**
 * 
 */
UCLASS()
class BOMBER_API ABomberRCBomb : public ABomberPickUp
{
	GENERATED_BODY()
	

public:
	// Sets default values for this actor's properties
	ABomberRCBomb();

	virtual void OnPickup(AActor* MyOverlappedActor, AActor* OtherActor) override;
	
	
};
