// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Bomber.h"
#include "GameFramework/Character.h" 
#include "Bomber.h"
#include "BomberCharacter.generated.h"

UCLASS(Blueprintable)
class ABomberCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABomberCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	float WalkSpeed;

	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool IsWalking() const;
	
	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool IsDroppingBomb() const;

	UFUNCTION(BlueprintCallable, Category = "Animation")
	void SetDroppingBomb(bool bValue);

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	void Die();

	virtual void PostInitializeComponents() override;

	void RecoverBomb();

	void IncrementBlastSize();

	void SetRunning();

	void RCBombPickedUp();

	UFUNCTION()
	void ClearRCBomb();

	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	int32 GetNumBombs();

	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	int32 GetNumPoints();

	UFUNCTION(BlueprintCallable, Category = "PlayerData")
	void GivePoints(int32 NumPoints);

	UFUNCTION()
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool IsDying() const;

	UFUNCTION(BlueprintCallable, Category = "Animation")
	bool HasWon() const;

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	bool HasRCBombs() const;

	UFUNCTION(BlueprintCallable, Category = Gameplay)
	float GetBlastSize() const;

	void SetWon(bool bValue);

	
private:

	UPROPERTY()
	float BlastSize;
	UPROPERTY()
	int32 NumBombs;
	UPROPERTY()
	int32 Points;
	UPROPERTY()
	bool bDroppingBomb;
	UPROPERTY()
	bool bDying;
	UPROPERTY()
	bool bWon;
	UPROPERTY()
	bool bRCBombs;
};

