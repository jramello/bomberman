// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BomberAIController.generated.h"

/**
 * 
 */
UCLASS()
class BOMBER_API ABomberAIController : public AAIController
{
	GENERATED_BODY()
	ABomberAIController();

	virtual void BeginPlay() override;
	
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "AI")
	bool SafeToMoveForward();

	UFUNCTION(BlueprintCallable, Category = "AI")
	void OnDropBomb();

protected:

	UPROPERTY(transient)
	UBlackboardData* BlackboardData;

	UPROPERTY(transient)
	UBehaviorTree* BotBehaviorTree;

private:

	FVector PreviousLocation;
	FRotator PreviousRotation;
};
