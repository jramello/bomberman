// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "GameFramework/Actor.h"
#include "BomberCharacter.h"
#include "DateTime.h"
#include "Sound/SoundCue.h"
#include "BomberPickUp.generated.h"

UCLASS()
class BOMBER_API ABomberPickUp : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberPickUp();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pickup")
	class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
	class USceneComponent* SceneComponent;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ParticleSystem", meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* ParticleSystemComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	FDateTime LifeTime;

	UFUNCTION()
	virtual void OnPickup(AActor* MyOverlappedActor, AActor* OtherActor);
	
};
