// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberPickUp.h"
#include "Components/StaticMeshComponent.h"
#include "BomberExplosion.h"


// Sets default values
ABomberPickUp::ABomberPickUp()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	OnActorBeginOverlap.AddDynamic(this, &ABomberPickUp::OnPickup);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SceneComponent->SetupAttachment(GetRootComponent());

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetupAttachment(SceneComponent);
	MeshComponent->bCastStaticShadow = false;
	MeshComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_MAX);

	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	ParticleSystemComponent->SetupAttachment(SceneComponent);
	ParticleSystemComponent->SetAutoActivate(true);
	ParticleSystemComponent->SecondsBeforeInactive = 0.0f;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> Particles(TEXT("ParticleSystem'/Game/Particles/P_Death_backpack_expl_02.P_Death_backpack_expl_02'"));
	if (Particles.Object)
	{
		ParticleSystemComponent->SetTemplate(Particles.Object);
	}

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Mesh(TEXT("StaticMesh'/Game/Geometry/Meshes/SM_PowerUpExtraBomb.SM_PowerUpExtraBomb'"));
	if (Mesh.Object)
	{
		MeshComponent->SetStaticMesh(Mesh.Object);
	}
}

// Called when the game starts or when spawned
void ABomberPickUp::BeginPlay()
{
	Super::BeginPlay();
	LifeTime = FDateTime::Now();
}

// Called every frame
void ABomberPickUp::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FRotator Rotation = GetActorRotation();
	Rotation.Yaw += 1.0f;
	SetActorRotation(Rotation);

}

void ABomberPickUp::OnPickup(AActor* MyOverlappedActor, AActor* OtherActor)
{
	ABomberExplosion* OverlappedExplosion = Cast<ABomberExplosion>(OtherActor);
	//Give invulnerability for 2 seconds to avoid being destroyed on spawn
	int32 TimeElapsed = FDateTime::Now().GetSecond() - LifeTime.GetSecond();
	if (OverlappedExplosion && TimeElapsed >= 2)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GEngine->GetWorld(), ParticleSystemComponent->Template, GetTransform());
		Destroy();
	}
}