// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberHUD.h"
#include "BomberCharacter.h"
#include "../UMG/Public/Blueprint/UserWidget.h"
#include "BomberUserWidget.h"
#include "../UMG/Public/Components/WidgetComponent.h"
#include "BomberEndGameUserWidget.h"

ABomberHUD::ABomberHUD()
{
	static ConstructorHelpers::FObjectFinder<UClass> ConstructScore(TEXT("Blueprint'/Game/UI/Actors/ScoreWidgetActor.ScoreWidgetActor_C'"));
	if (ConstructScore.Object != NULL)
	{
		ScoreBP = (UClass*)ConstructScore.Object;
	}

}

void ABomberHUD::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ABomberHUD::DrawHUD()
{
	Super::DrawHUD();
}


void ABomberHUD::ShowEndGame(int32 Id)
{
	if (EndGameWidget)
	{
		UBomberEndGameUserWidget* MyEndGameWidget = CreateWidget<UBomberEndGameUserWidget>(GetGameInstance(), EndGameWidget);
		MyEndGameWidget->WinnerId = Id;
		MyEndGameWidget->UpdateWidget();
		MyEndGameWidget->AddToViewport();
	}
}

void ABomberHUD::ShowControls()
{
	if (ControlsWidget)
	{
		UBomberUserWidget* MyControlsWidget = CreateWidget<UBomberUserWidget>(GetGameInstance(), ControlsWidget);
		MyControlsWidget->AddToViewport();
	}
}

void ABomberHUD::ShowMainMenu()
{
	if (MainMenuWidget)
	{
		UBomberUserWidget* MyMainMenuWidget = CreateWidget<UBomberUserWidget>(GetGameInstance(), MainMenuWidget);
		MyMainMenuWidget->AddToViewport();
	}
}


void ABomberHUD::ShowIngameHUD()
{
	if(GUIWidget) 
	{
		UBomberUserWidget* MyGUI = CreateWidget<UBomberUserWidget>(GetGameInstance(), GUIWidget);
		MyGUI->AddToViewport();
	}
}


void ABomberHUD::ShowPoints(int32 Points, FVector Location)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = GetOwner();
	AActor* MyScore = GetWorld()->SpawnActor<AActor>(ScoreBP, Location, FRotator::ZeroRotator, SpawnInfo);

	if (MyScore)
	{
		UActorComponent* ActorComponent = MyScore->GetComponentByClass(UWidgetComponent::StaticClass());
		UWidgetComponent* WidgetComponent = Cast<UWidgetComponent>(ActorComponent);
		UBomberUserWidget* MyScoreWidget = Cast<UBomberUserWidget>(WidgetComponent->GetUserWidgetObject());
		MyScoreWidget->NumericParameter = Points;
	}
}