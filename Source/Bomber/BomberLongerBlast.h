// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "BomberPickUp.h"
#include "BomberLongerBlast.generated.h"

/**
 * 
 */
UCLASS()
class BOMBER_API ABomberLongerBlast : public ABomberPickUp
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABomberLongerBlast();

	virtual void OnPickup(AActor* MyOverlappedActor, AActor* OtherActor) override;
	
	
	
	
};
