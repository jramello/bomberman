// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Particles/ParticleSystemComponent.h"
#include "BomberCharacter.h"
#include "BomberExplosion.generated.h"

UCLASS()
class BOMBER_API ABomberExplosion : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberExplosion();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Explosion")
	void DamageCharacter(ABomberCharacter* DamagedCharacter);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	
};
