// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "GameFramework/HUD.h"
#include "BomberHUD.generated.h"

UCLASS()
class BOMBER_API ABomberHUD : public AHUD
{
	GENERATED_BODY()
	
protected:

	virtual void PostInitializeComponents() override;

public:

	UPROPERTY(EditAnywhere, Category = General)
	TSubclassOf<class AActor> ScoreBP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> GUIWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> EndGameWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> ControlsWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> PointsWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> MainMenuWidget;

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void ShowIngameHUD();

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void ShowEndGame(int32 Id);

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void ShowControls();

	UFUNCTION(BlueprintCallable, Category = "HUD")
	void ShowMainMenu();

	ABomberHUD();

	void ShowPoints(int32 Points, FVector Location);
};
