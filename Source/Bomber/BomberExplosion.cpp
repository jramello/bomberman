// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberExplosion.h"

// Sets default values
ABomberExplosion::ABomberExplosion()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABomberExplosion::BeginPlay()
{
	//Size = Cast<ABomberCharacter>(GetOwner())->GetBlastSize();
	Super::BeginPlay();
}

// Called every frame
void ABomberExplosion::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABomberExplosion::DamageCharacter(ABomberCharacter* DamagedCharacter)
{
	FDamageEvent DefaultDamage;
	
	DamagedCharacter->TakeDamage(0.0f, DefaultDamage, GetInstigatorController(), GetOwner());
}
