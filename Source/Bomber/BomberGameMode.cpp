// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BomberGameMode.h"
#include "BomberPlayerController.h"
#include "BomberCharacter.h"
#include "BomberMapTile.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "BomberHUD.h"
#include "TimerManager.h"
#include "BomberGameInstance.h"

ABomberGameMode::ABomberGameMode()
{
	HUDClass = ABomberHUD::StaticClass();
	// use our custom PlayerController class
	PlayerControllerClass = ABomberPlayerController::StaticClass();
	NumBots = 0;
	RoundTime = 30;
	bGameEnded = false;

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/BP_BomberCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
	//Store map tile blueprint for map generation
	static ConstructorHelpers::FObjectFinder<UClass> MapTileBP(TEXT("Blueprint'/Game/Blueprints/Map/BP_MapTile.BP_MapTile_C'"));
		if (MapTileBP.Object) 
	{
		MapTile = (UClass*)MapTileBP.Object;
	}
}

void ABomberGameMode::BeginPlay()
{
	//Generating map on begin play with hardcoded size
	//TODO: Parametrize map sizes
	GenerateMap(10);

	//Create P2 and manually spawn him if there are no bots.
	//TODO: Custom player start points
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetPawn()->SetActorLocation(FVector(100, 100, 100));
	if(NumBots == 0)
	{	
		APlayerController* NewPlayer = UGameplayStatics::CreatePlayer(GetWorld(), 1);
		UGameplayStatics::GetPlayerController(GetWorld(), 1)->GetPawn()->SetActorLocation(FVector(900, 900, 100));
	}

	//Decrement round timer
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;
	TimerDel.BindUFunction(this, FName("UpdateTimer"));
	GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, 1.0f, true);


	ABomberHUD* MyHUD = Cast<ABomberHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	MyHUD->ShowControls();

}

void ABomberGameMode::SpawnBot()
{
	UWorld* World = GetWorld();
	if (World)
	{
		UBomberGameInstance* MyGameInstance = Cast<UBomberGameInstance>(GetGameInstance());
		ABomberAIController* BomberAIController = CreateBot(CurrentNumBots++, DefaultPawnClass);
		RestartPlayer(BomberAIController);
		BomberAIController->GetPawn()->SetActorLocation(FVector(900, 900, 100));
	}
}

ABomberAIController* ABomberGameMode::CreateBot(int32 BotNum, TSubclassOf<APawn> CharacterClass)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Instigator = nullptr;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.OverrideLevel = nullptr;
	AController* NewController = nullptr;

	ACharacter* MyPawn = Cast<ACharacter>((*CharacterClass)->GetDefaultObject<ACharacter>());
	NewController = GetWorld()->SpawnActor<AController>(ABomberAIController::StaticClass(), SpawnInfo);

	return Cast<ABomberAIController>(NewController);
}


int32 ABomberGameMode::GetTimeRemaining()
{
	return RoundTime;
}

void ABomberGameMode::UpdateTimer()
{
	RoundTime--;
	if (RoundTime == 0 && !bGameEnded)
	{
		//Stop input
		FInputModeUIOnly Mode;
		GetWorld()->GetFirstPlayerController()->SetInputMode(Mode);
		//Draw when time is 0
		EndGame(0);
	}
}

void ABomberGameMode::EndGame(int32 Winner)
{
	ABomberHUD* MyHUD = Cast<ABomberHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	MyHUD->ShowEndGame(Winner);
	bGameEnded = true;
}

void ABomberGameMode::GenerateMap(int Size)
{
	for (int32 i = 0; i <= Size; i++)
	{
		for(int32 j = 0; j <= Size; j++)
		{

		FActorSpawnParameters SpawnInfo;
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		FVector TileLocation = FVector(j * (100), i * (100), 0);
		ABomberMapTile* MyMapTile = GetWorld()->SpawnActor<ABomberMapTile>(MapTile, TileLocation, FRotator::ZeroRotator);

		if (i == 0 || j == 0 || i == Size || j == Size)
		{
			//Border walls are indestructible always
			MyMapTile->WallType = EWallType::Indestructible;
		}
		else
		{
			if ((i == 1 && j == 1) || (i == 1 && j == 2) || (i == 2 && j == 1) ||
				(i == Size - 1 && j == Size - 1) || (i == Size - 1 && j == Size - 2) || (i == Size - 2 && j == Size - 1))
			{
				//Make sure corners are never filled with any kind of wall
				MyMapTile->WallType = EWallType::None;
			}
			else if (i % 2 == 0 && j % 2 == 0)
			{
				MyMapTile->WallType = EWallType::Indestructible;
			}
			else
			{
				float RandomizeTile = FMath::RandRange(0.0f, 1.0f);
				//20% of map tiles are destructible
				//TODO: Parametrize this value
				if (RandomizeTile > 0.8f)
				{
					MyMapTile->WallType = EWallType::Destructible;
				}
				else
				{
					MyMapTile->WallType = EWallType::None;
				}
			}
		}

		MyMapTile->InitializeWallType();
		}
	}
}

int32 ABomberGameMode::GetNumBots()
{
	return NumBots;
}

FString ABomberGameMode::InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal)
{
	FString ErrorMsg = Super::InitNewPlayer(NewPlayerController, UniqueId, Options, Portal);

	if (UGameplayStatics::HasOption(Options, "NumBots"))
	{
		FString Str = UGameplayStatics::ParseOption(Options, "NumBots");
		NumBots = FCString::Atoi(*Str);
		//Spawn bot number depending on the arguments given
		for (int32 i = 0; i < NumBots && NumBots > CurrentNumBots; i++)
		{
			SpawnBot();
		}
	}
	return ErrorMsg;
}

void ABomberGameMode::RestartPlayer(class AController* NewPlayer)
{
	Super::RestartPlayer(NewPlayer);
}

void ABomberGameMode::EndingGame()
{
	//Wait 2 seconds before announcing the winner to give a chance to draw after a player has been killed.
	//TODO: Trigger if map timer reaches 0
	FTimerDelegate TimerDel;
	FTimerHandle TimerHandle;
	if (!TimerDel.IsBound())
	{
		TimerDel.BindUFunction(this, FName("CheckWinner"));
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, TimerDel, 2.0f, false);
	}
}

void ABomberGameMode::CheckWinner()
{
	if(!bGameEnded)
	{
		int32 Winner = -1;

		for (TActorIterator<AController> It(GetWorld()); It; ++It)
		{
			AController* Controller = *It;
			if (Controller)
			{
				ABomberCharacter* MyPawn = Cast<ABomberCharacter>(Controller->GetPawn());
				// If there is someone alive, he is the winner
				if (!MyPawn->IsDying())
				{
					MyPawn->SetWon(true);
					ABomberPlayerController* MyPC = Cast<ABomberPlayerController>(Controller);
					ABomberAIController* MyAIPC = Cast<ABomberAIController>(Controller);
					if (MyPC)
					{
						FInputModeUIOnly Mode;
						MyPC->SetInputMode(Mode);
						Winner = UGameplayStatics::GetPlayerControllerID(MyPC);
					}
					else if (MyAIPC)
					{
						//Bot is always ID 1
						Winner = 1;
					}
					Cast<UBomberGameInstance>(GetGameInstance())->LastScore = MyPawn->GetNumPoints();					
				}
			}
		}
		EndGame(Winner + 1);
	}
}