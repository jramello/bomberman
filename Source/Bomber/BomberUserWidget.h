// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "Blueprint/UserWidget.h"
#include "BomberUserWidget.generated.h"

UCLASS()
class BOMBER_API UBomberUserWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	virtual void NativeConstruct() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget Data")
	int32 NumericParameter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widget Data")
	FString StringParameter;
	
	
};
