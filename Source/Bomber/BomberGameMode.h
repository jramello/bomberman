// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Bomber.h"
#include "GameFramework/GameModeBase.h"
#include "BomberAIController.h"
#include "BomberGameMode.generated.h"


UCLASS(minimalapi)
class ABomberGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ABomberGameMode();

	/** start match, or let player enter, immediately */
	virtual FString InitNewPlayer(APlayerController* NewPlayerController, const FUniqueNetIdRepl& UniqueId, const FString& Options, const FString& Portal = TEXT("")) override;

	/** Does end of game handling for the online layer */
	virtual void RestartPlayer(class AController* NewPlayer) override;

	TSubclassOf<class ABomberMapTile> MapTile;

	void EndingGame();

	UFUNCTION(BlueprintCallable, Category = "Timer")
	int32 GetTimeRemaining();

	UFUNCTION(BlueprintCallable, Category = "Bot")
	void SpawnBot();

	UFUNCTION(BlueprintCallable, Category = "Bot")
	int32 GetNumBots();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GameState")
	bool bGameEnded;


protected:

	UFUNCTION()
	void CheckWinner();

	UFUNCTION()
	void UpdateTimer();

	// Called when the game starts
	virtual void BeginPlay() override;


private:

	void EndGame(int32 Winner);

	int32 RoundTime;

	int32 NumBots;

	int32 CurrentNumBots;

	void GenerateMap(int Size);

	/** Create a bot */
	ABomberAIController* CreateBot(int32 BotNum, TSubclassOf<APawn> CharacterClass);

};



