// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "Bomber.h"
#include "BomberMenuGameMode.generated.h"

UCLASS()
class BOMBER_API ABomberMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()


public:
	ABomberMenuGameMode();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void SetNumBots(int32 Num);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	int32 GetNumBots();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
private:

	int32 NumBots;

	void RemoveSplitscreenPlayers();

};
