// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberAIController.h"
#include "Bomber.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h"
#include "BomberCharacter.h"
#include "BomberExplosion.h"
#include "BomberBomb.h"

ABomberAIController::ABomberAIController()
{
	static ConstructorHelpers::FObjectFinder<UBlackboardData> ConstructorBlackBoard(TEXT("BlackboardData'/Game/AI/BB_BomberAI.BB_BomberAI'"));
	static ConstructorHelpers::FObjectFinder<UBehaviorTree> ConstructorBehaviourTree(TEXT("BehaviorTree'/Game/AI/BomberBehaviorTree.BomberBehaviorTree'"));
	Blackboard = CreateDefaultSubobject<UBlackboardComponent>("Blackboard");
	BlackboardData = ConstructorBlackBoard.Object;
	BotBehaviorTree = ConstructorBehaviourTree.Object;
	bAllowStrafe = false;
}

void ABomberAIController::BeginPlay()
{
	Super::BeginPlay();
	Blackboard->InitializeBlackboard(*BlackboardData);
	RunBehaviorTree(BotBehaviorTree);
}

void ABomberAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

bool ABomberAIController::SafeToMoveForward()
{
	bool bSafe = true;
	if(GetPawn())
	{
		FVector Destination = FVector::ZeroVector;
		FVector ForwardVector = GetActorForwardVector();
		FVector StartTrace = GetPawn()->GetActorLocation();
		StartTrace.Z -= 80.0f;

		//Check that there is not an explosion, a bomb or a character in front of you
		float Distance = 300.0f;

		FVector EndTrace = StartTrace + ForwardVector * Distance;
		//DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor::Red, false, 1.0f, 1, 4.0f);
		FHitResult Hit(ForceInit);
		FCollisionQueryParams TraceParams(FName(TEXT("ViewTrace")), true);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.AddIgnoredActor(GetPawn());
		GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, ECC_MAX, TraceParams);
		
		if (Hit.bBlockingHit && (Cast<ABomberExplosion>(Hit.GetActor()) || Cast<ABomberCharacter>(Hit.GetActor()) || Cast<ABomberBomb>(Hit.GetActor())))
		{
			bSafe = false;
		}
	}
	return bSafe;
}

void ABomberAIController::OnDropBomb()
{
	ABomberCharacter* MyPawn = Cast<ABomberCharacter>(GetPawn());
	if (MyPawn)
	{
		bool RCBombsExploded = false;
		//If player has RC bombs powerup, make his bombs explode instead of spawning a new one.
		if (MyPawn->HasRCBombs())
		{
			for (TActorIterator<ABomberBomb> It(GetWorld()); It; ++It)
			{
				ABomberBomb* Bomb = *It;
				if (Bomb && Bomb->GetOwner() == MyPawn)
				{
					RCBombsExploded = true;
					Bomb->Destroy();
				}
			}
		}
		if (!RCBombsExploded)
		{
			MyPawn->SetDroppingBomb(true);
		}
	}
}