// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bomber.h"
#include "Engine/GameInstance.h"
#include "Ticker.h"
#include "BomberGameInstance.generated.h"


namespace BomberGameInstanceState
{
	const FName None = FName(TEXT("None"));
	const FName MainMenu = FName(TEXT("MainMenu"));
	const FName Playing = FName(TEXT("Playing"));
}

UCLASS()
class BOMBER_API UBomberGameInstance : public UGameInstance
{
	GENERATED_BODY()
private:
	void MaybeChangeState();
	void EndCurrentState(FName NextState);
	void BeginNewState(FName NewState, FName PrevState);

	void BeginMainMenuState();
	void BeginPlayingState();

	void EndMainMenuState();
	void EndPlayingState();

	void OnPreLoadMap(const FString& MapName);
	void OnPostLoadMap();


	bool bReadyToChange;

	bool bFadingOut;

	UPROPERTY()
	FString SelectedMapName;

	UFUNCTION()
	void SetReadyToChange();
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int32 LastScore;

	UBomberGameInstance();

	UFUNCTION()
	void SetSelectedMap(FString MapName);

	FName CurrentState;

	FName PendingState;

	virtual void Init() override;

	bool Tick(float DeltaSeconds);

	virtual void StartGameInstance() override;

	/** Delegate for callbacks to Tick */
	FTickerDelegate TickDelegate;

	/** Handle to various registered delegates */
	FDelegateHandle TickDelegateHandle;

	/** Sends the game to the specified state. */
	UFUNCTION(BlueprintCallable, Category = "State")
	void GotoState(FName NewState);
	
	UFUNCTION(BlueprintCallable, Category = "State")
	void NextRound(int32 Bots);

	void ChangeMap(FString MapName, int32 Bots);

	/** Obtains the initial welcome state, which can be different based on platform */
	FName GetInitialState();

	/** Sends the game to the initial startup/frontend state  */
	void GotoInitialState();

	/** Quits the game */
	void Quit();
	
	
};
