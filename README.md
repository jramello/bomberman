# Bomberman

DEVELOPING A BOMBERMAN GAME
==============

By Javier A. Ramello Marchioni
--------------

THIRD PARTY CONTENT:
� Character models and animations -> https://www.assetstore.unity3d.com/en/#!/content/49268
� Bomb model and textures -> https://www.turbosquid.com/3d-models/cartoon-bomb-obj-free/1034107
� Particle effects borrowed from UE4 content examples
� Sound effects -> https://downloads.khinsider.com/bomberman
� Original Bomberman Soundtrack -> https://www.sounds-resource.com/pc_computer/bombermancollection/sound/772/

WHAT I WOULD DO NEXT:
� Adding support for more bots
� Adding random enemies
� Bot AI coud use a lot more work
� Randomize the size of the maps
� Proper score rankings
� Localization

KEYBOARD CONTROLS GUIDE
� PLAYER 1 -> WASD (Movement) Left Ctrl (Drop Bomb)
� PLAYER 2 -> UP/DOWN/LEFT/RIGHT (Movement) Num 0 (Drop Bomb)

TIME
� (20h) The basic game would have been done in the estimated time, however I took a bit more time in order to add the animations, particles, menus, etc. 
� Spent the first day dealing with a messy Visual Studio update (Reason why the project is in UE4 v4.16, sorry about that).

NOTES
� The bonus dynamic camera works, but has been disabled in single player mode.
� There is a build of the game zipped in the root folder "BomberBuild.rar"